pyrate package
==============

Subpackages
-----------

.. toctree::

    pyrate.algorithms
    pyrate.chains
    pyrate.classes
    pyrate.histograms
    pyrate.jobs

Submodules
----------

pyrate.skeleton module
----------------------

.. automodule:: pyrate.skeleton
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pyrate
    :members:
    :undoc-members:
    :show-inheritance:
