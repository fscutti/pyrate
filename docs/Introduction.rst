Introduction
============

The pyrate framework is software developed for the `SABRE dark-matter experiment <http://sabre.lngs.infn.it/>`_.
It allows for a variety of operations common while handling data in particle physics experiments and at most 
levels of the workflow, while being sufficiently general to be used in conjunction with other software packages.
Generally, it supports the following primary operations:

* Event recontruction.
* Data format transformation.
* Data analysis and statistical inference.
* Display of graphics/results, like plotting or event display implementations.
* python wrapper for other software or algorithms.

Given the high popularity of `ROOT <https://root.cern.ch/>`_ in the particle physics community to transform and
analyse data, it is important to keep in mind that this will be an important dependency of pyrate itself through
the `PyROOT <https://root.cern.ch/pyroot>`_ extension module.

Philosophy
----------

In particle physics the primary operation we are interested in is running event loops. For this reason the skeleton 
structure of pyrate is a Job object implementing an event loop algorithm. The Job can be configured with input files 
and data structures which the user defined. A list of algorithms can be loaded into the job which are run sequentially 
and can perform operations like computation of variables or event selection. A set of file-reading engines called FileReaders 
are provided to the job, rather than just a list of input file names. These readers provide the necessary facilities
to push the event loop forward on an event by event basis and hold the main structure of the event in memory refreshing
its content in the loop. They are essentially an interface between the input files and the user-defined algorithms.
The event content is accessible to the algorithms through these readers which are attributes of the algorithms and 
dispatched to them by the main job. The event loop itself is implemented as a nested loop over a list of events within 
a loop over a list of readers. The purpose of the outer loop is to load data only when needed and to initialise the
algorithms in the order they where declared. Algorithms are finalised in order once the inner loop (events) is over. 
Algorithms are executed in order within the inner event loop.

Algorithms manage two kinds of memory caches. A persistent one holding objects for the entire job execution, and a 
transient one holding information only on an event-by-event basis. These memory caches are shared among all algorithms 
which can use them to retrieve or store data. As previously mentioned, user-defined algorithms participate the global
event loop by implementing public initialise, execute and finalise methods.

Sometimes, it is not necessary to run any loop for the operations we need. An example is plotting from input files 
containing just histograms for different variables and from different physical sources. In this case the Job can be
configured to skip the event loop altoghether and simply call the initialise, execute and finalise algorithm methods
in order after the necessary data are loaded into memory.

.. note::
   The choice of python over other languages allows for an easier to use and maintain software. Where speed will be
   a concern, a specific pyrate version will be developed, relying on `pypy <http://pypy.org/>`_ a particular 
   implementation of the language. Python 2.7 has been used for development. Given the limited lifetime of 
   `python 2 <https://pythonclock.org/>` a migration will be necessary in the future if 
   `pyrate dependencies <https://python3statement.org/>`_ are found to be broken.


Development branches
--------------------

Dependencies
------------

Structure
---------

.. toctree::
   :maxdepth: 2

   Classes
   Jobs
   Algorithms
   Histograms
   Chains




