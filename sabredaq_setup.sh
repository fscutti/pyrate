## ========================
## Setup script for running 
## on the sabredaq machine.
## ========================

## ----------------------
## pre-setup, don't touch
## ----------------------

path_of_this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export MAIN=${path_of_this_dir}

add_to_python_path()
{
    export PYTHONPATH=$1:$PYTHONPATH
    echo "  Added $1 to your PYTHONPATH."
}

add_to_path()
{
    export PATH=$1:$PATH
    echo "  Added $1 to your PATH."
}

## ----------------
## setup PYTHONPATH
## ----------------

echo "  Setting up your PYTHONPATH."
add_to_python_path ${MAIN}
#add_to_python_path ${MAIN}/pyrate
echo "  done."

## ---------------------------------------------
## Add sabre_muons_daq/scripts directory to PATH
## ---------------------------------------------
#echo "  Add scripts to PATH."
#add_to_path ${MAIN}/scripts
#echo "  done."

export DAQ_SCRIPTS=$(pwd)/scripts
if [ -n "${PATH}" ]; then
    export PATH=${DAQ_SCRIPTS}:${PATH}
else
    export PATH=${DAQ_SCRIPTS}
fi

## ----------------
## python utilities
## ----------------

pip install --user file_read_backwards
pip install --user tqdm
pip install --user colorama
pip install --user scipy

#EOF
