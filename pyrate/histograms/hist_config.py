#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
This file holds the configuration of all histograms.
The vname string should correspond to the name of a
variable in the store used to fill the histograms.
"""
from pyrate.classes import Hist1D

test_hist = Hist1D(name   = "test_hist",
                 nbins  = 100,
                 xmin   = 0,
                 xmax   = 100,
                 vname  = "evnumber",
                 xtitle = "Random Variable",
                 ytitle = "Entries",
                )

wmax_ch0_hist = Hist1D(name   = "waveform_max",
                 nbins  = 1000,
                 xmin   = -1,
                 xmax   = 1,
                 vname  = "wmax_ch0",
                 xtitle = "Waveform maximum [CH0]",
                 ytitle = "Entries",
                )

unixtime_hist = Hist1D(name   = "unixtime",
                 nbins  = 1000,
                 xmin   = 1554948550.000,
                 xmax   = 1554948551.000,
                 vname  = "unixtime",
                 xtitle = "UnixTime [s]",
                 ytitle = "Entries",
                )

tdc_corr_hist = Hist1D(name   = "tdc_corrected",
                 nbins  = 100,
                 xmin   = 0,
                 xmax   = 100,
                 vname  = "tdc_corrected",
                 xtitle = "TDC corrected",
                 ytitle = "Entries",
                )

event_number_hist = Hist1D(name   = "event_number",
                 nbins  = 80000,
                 xmin   = 0,
                 xmax   = 8000000,
                 vname  = "eventnumber",
                 xtitle = "Event number",
                 ytitle = "Entries",
                )




# EOF

