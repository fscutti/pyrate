#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

from pyrate.algorithms import var_algs
from pyrate.histograms import hist_config

"""
This is just a test file, intended for showing a basic configuration.
"""

# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM     = "wd"

INPATH   = "/home/cpyke/test_files"
#INPATH   = "/home/fscutti/WDfiles"

ch0 = Run(name = "Channel_0", form=FORM, infile = os.path.join(INPATH,"wave0.txt"),  min_idx_event = EMIN, max_idx_event = EMAX)
ch1 = Run(name = "Channel_1", form=FORM, infile = os.path.join(INPATH,"wave1.txt"),  min_idx_event = EMIN, max_idx_event = EMAX)
ch2 = Run(name = "Channel_2", form=FORM, infile = os.path.join(INPATH,"wave2.txt"),  min_idx_event = EMIN, max_idx_event = EMAX)
ch3 = Run(name = "Channel_3", form=FORM, infile = os.path.join(INPATH,"wave3.txt"),  min_idx_event = EMIN, max_idx_event = EMAX)

# List of runs:
# -------------
# This is the list of runs considered for the job. They should each correspond to a different input file.
# An option can be passed whether to read the run info for WaveDump acquisitions.

run_list = []
run_list.append( Run(name = "run_WD", read_run_info = False, channels = [ch0,ch1,ch2,ch3]))

# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

job = Job(name="job_WD", run_list=run_list, log_lvl = log_lvl)
job += var_algs.CalcVars()
job += var_algs.CoincidenceFinder()

# EOF
