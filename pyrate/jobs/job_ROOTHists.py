#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import logging

from pyrate.classes  import Run
from pyrate.classes  import Job

from pyrate.algorithms import ROOT_algs, var_algs
#from pyrate.histograms import hist_config

"""
This is just a test file, intended for showing a basic configuration.
"""


# log level
# ---------
# Select the severity of logging info

log_lvl = logging.DEBUG

# min and max events: 
# -------------------
# by default are [min_idx_event, max_idx_event = 0, -1] will read all available events.
# The user is free to choose an index interval (index in [0, #events-1]). If the index 
# is inconsistent with the #events an error will appear printing the last event info.

EMIN     = 0
EMAX     = -1

# input file format:
# ------------------
# wc and wd indicate a WaveCatcher and WaveDump format respectively

FORM     = "root"

# input/output files:
# -------------------
# Absolute paths of the input and output files

INPATH   = "/home/cpyke/test_files/plotting/trees/Lumirror_Background"

OUTPATH  = "/home/cpyke/test_output"
OUTFILE  = os.path.join(OUTPATH,"outroot.root")

# List of histograms:
# -------------------
# Histograms to be included in the job should be appended to this list
# and configured in hist_config.

"""
hlist = []
hlist.append(hist_config.wmax_ch0_hist)
#hlist.append(hist_config.unixtime_hist)
hlist.append(hist_config.tdc_corr_hist)
"""

Ac228 = Run(name = "Ac228", form = FORM, infile = os.path.join(INPATH,"Lumirror_Ac228_part*.root"), input_hists="histo", style = "Red") 
Pb214 = Run(name = "Pb214", form = FORM, infile = os.path.join(INPATH,"Lumirror_Pb214_part*.root"), input_hists="histo", style = "Blue") 
K40   = Run(name = "K40",   form = FORM, infile = os.path.join(INPATH,"Lumirror_K40_part*.root"),   input_hists="histo", style = "Green") 

sample_list = []
sample_list.append( Ac228 )
sample_list.append( Pb214 )
sample_list.append( K40 )


# instantiate the job:
# --------------------
# This job instance will be launched in the main script. After passing a list of runs 
# algorithms are added to the job. 
# WARNING: the order of the algorithms matters!!!  E.g. define a variable first if you
# want to retrieve it later on.

#"""
job = Job(name = "job_ROOT_Hists", run_list = sample_list, log_lvl = log_lvl, is_loop=False)
job += var_algs.CalcVars()
#job += ROOT_algs.FillROOTHists(hlist = hlist)
#job += ROOT_algs.WriteROOTFile(outfile = OUTFILE, hist_list=hlist, opt="RECREATE")
#"""
# EOF
