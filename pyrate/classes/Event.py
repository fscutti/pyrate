#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class handling events.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"


import sys
import collections
import pprint


class Proxy(object):
  """
  Event and RunInfo inherit from this generic object.
  """

  def __init__(self, form, **kwargs):
   
    # ----------------------------
    # set additional key-word args
    # ----------------------------
    
    for k,w in kwargs.iteritems():
        setattr(self, k, w)
    
    self.form = form

  def print_info(self, **kw):
    """
    Print out the attributes of the object.
    Mostly used for debugging.
    """
    if kw: pprint.pprint(kw)
    pprint.pprint(self.__dict__)
    print  

  def extend(self, **kw):
    """
    Extend the attributes of the object.
    """
    
    for k,v in kw.iteritems():
      if not hasattr(self, k): setattr(self, k, v)
    
    return 


class Event(Proxy):
  """
  This class represents the event known to the Run or the Channel,
  which both inherit from the FileReader. One instance is created
  while reading the run, and its attributes are updated while reading
  the file.
  """
  
  def __init__(self, form, **kwargs):
    Proxy.__init__(self, form, **kwargs)
    
    # ------------------------------------    
    # format of event from wave-dump files
    # ------------------------------------
    if self.form == "wd":
      self.header         = {"Record Length"     : None, 
                             "BoardID"           : None, 
                             "Channel"           : None, 
                             "Event Number"      : None, 
                             "Pattern"           : None, 
                             "Trigger Time Stamp": None, 
                             "DC offset (DAC)"   : None,
                             }
      
      self.size             = self.record_length + len(self.header)
      self.broken_size      = 0
      self.has_header       = len(self.header) > 0
      self.waveform         = []
      self.idx_event        = -1
      self.wf_is_broken     = False
      self.header_is_broken = False
         


    # ---------------------------------------    
    # format of event from wave-catcher files
    # ---------------------------------------
    elif self.form == "wc":
      self.header         = {"EVENT"        : None, 
                             "UnixTime"     : None, 
                             "date"         : None, 
                             "time"         : None, 
                             "TDC From FPGA": None, 
                             "TDC Corrected": None, 
                             }

      # info relative to single channel in the event
      self.cinfo = collections.OrderedDict()
      
      self.cinfo["CH"]               = None, 
      self.cinfo["EVENTID"]          = None, 
      self.cinfo["FCR"]              = None, 
      self.cinfo["Baseline"]         = None, 
      self.cinfo["Amplitude"]        = None, 
      self.cinfo["Charge"]           = None, 
      self.cinfo["LeadingEdgeTime"]  = None,
      self.cinfo["TrailingEdgeTime"] = None,
      self.cinfo["TrigCount"]        = None,
      self.cinfo["TimeCount"]        = None,
      self.cinfo["waveform"]         = [],
      
      self.idx_event      = -1
      
      self.cdict = collections.OrderedDict()
    
    # -------------------------------    
    # format of event from root files
    # -------------------------------
    elif self.form == "root":
      self.idx_event      = -1
      if "chain" in self.__dict__: pass


    # --------------------------------    
    # format of event from credo files
    # --------------------------------
    if self.form == "cr":
      self.header          = None
      
      self.lts             = None
      self.rts             = None
      self.lbs             = None
      self.rbs             = None
      self.xcoord          = None
      self.ycoord          = None
      self.idx_event       = -1


  def update(self, **kw):
    """
    Update the attributes of the event.
    """
    
    for k,v in kw.iteritems():
      if hasattr(self, k): setattr(self, k, v)
    
    return 
 

  def validate(self, **kw):
    """
    Validate the event after updating the attributes.
    """
    is_good = True

    event_idx = self.idx_event
    
    if event_idx < 0:
      event_idx = "last"
    
    # ------------------------------------    
    # event validation for wave-dump files
    # ------------------------------------
    if self.form == "wd":
      
      """ 
      if not len(self.waveform) == self.record_length:
        sys.exit("ERROR: wavelength size is wrong for {} event".format(event_idx))
      """ 
      
      if self.has_header:
        if not all(self.header.values()):
          #self.print_info()
          is_good = False
          #sys.exit("ERROR: header is incomplete for {} event".format(event_idx))
      
      if not self.size == len(self.waveform) + len(self.header):
        #self.print_info()
        is_good = False
        #sys.exit("ERROR: event size not consistent for {} event!".format(event_idx))
   

    # ---------------------------------------    
    # event validation for wave-catcher files
    # ---------------------------------------
    elif self.form == "wc":
      
      if not len(self.cdict) == int(kw["run_info"].info["NB OF CHANNELS ACQUIRED"]):
         self.print_info()
         sys.exit("ERROR: # of channels acquired is {} for event idx {}, but should be {}".format(len(self.cdict), event_idx, int(kw["run_info"].info["NB OF CHANNELS ACQUIRED"])))
    
      for ch,info in self.cdict.iteritems():
        if not str(len(info["waveform"]))+" in Volts" in kw["run_info"].info["SAMPLES"]:
          self.print_info()
          sys.exit("ERROR: waveform size not consistent for {} in event number {}!".format(ch,event_idx+1))
        
        if not event_idx  == "last":
          n_event = event_idx + 1
          if not n_event == int(info["EVENTID"]):
            self.print_info()
            sys.exit("ERROR: event index is {} but event number is {} for channel {}".format(event_idx, info["EVENTID"], ch))

    else: pass

    return is_good


class RunInfo(Proxy):
  """
  This class represents the RunInfo known to the Run or the Channel,
  which both inherit from the FileReader. One instance is created
  while reading the run, and its attributes are updated just once 
  for each file.
  """
  
  def __init__(self, form, **kwargs):
    Proxy.__init__(self, form, **kwargs)

    # ---------------------------------------    
    # format of run-info from wave-dump files
    # ---------------------------------------
    if self.form == "wd":
      self.info         = {"Trg Rate"     : None, 
                           "Run Start"    : None, 
                           "Run End"      : None, 
                           }


    # ------------------------------------------    
    # format of run-info from wave-catcher files
    # ------------------------------------------
    elif self.form == "wc":
      self.info         = {"SOFTWARE VERSION"       : None, 
                           "BOARD TYPE"             : None, 
                           "SAMPLES"                : None, 
                           "NB OF CHANNELS ACQUIRED": None, 
                           "Sampling Period"        : None, 
                           "INL Correction"         : None, 
                           }
    
    # -----------------------------------    
    # format of run-info from CREDO files
    # -----------------------------------
    elif self.form == "cr":
      self.info         = {"columns"       : None, 
                           "readings"      : None, 
                           }


    return 

# EOF
