#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class with the main body of a generic algorithm.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"


class Algorithm(object):
  """
  Algorithms will inherit from this class. In the derived __init__
  method, the user defines specific attributes of the algorithm defined.
  Within all methods of this class the current event in the loop can be
  accessed using run.event (WaveCatcher) or run.channel[idx].event (WaveDump).
  Similarly for other run properties.
  """
  
  def __init__(self, name="Algorithm", run=None, logger=None):

    """
    The user should load here all the main data structures 
    needed by an algorithm.
    """
    
    self.name      = name or self.__class__
    self.run       = run
    self.tr_store  = dict()
    self.pr_store  = dict()
    self._parent   = None
    self.logger    = logger

  def initialize(self):
    """
    Override this method in your derived class as you need.
    This method is executed after loading the run and a new time
    for every new run if they are loaded sequentially.
    """
    pass
  
  def execute(self):
    """
    Override this method in your derived class as you need.
    This method is executed for every event.
    """
    pass
 
  def finalize(self):
    """
    Override this method in your derived class as you need.
    This method is executed after running on all events,
    at the very end of every loop.
    """
    pass
  
# EOF 
