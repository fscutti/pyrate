#!/usr/bin/env python
# -*- coding: utf-8 -*-

description="""
Python class handling the run.
"""

__author__     = "Federico Scutti"
__email__      = "federico.scutti@unimelb.edu.au"
__maintainer__ = "Federico Scutti"
__date__       = "April 2019"


import sys
from copy import copy, deepcopy
from FileReader import FileReader
from Event import Event, RunInfo

class Run(FileReader):

  def __init__(self, 
               infile        = None,
               form          = None,
               read_run_info = True,
               name          = "Run",
               min_idx_event = 0,
               max_idx_event = -1,
               channels      = [],
               **kwargs
              ):
    
    self.name          = name or infile
    self.form          = form
    self.read_run_info = read_run_info
    self.channels      = channels
    
    if not self.channels: FileReader.__init__(self, infile, form, read_run_info, min_idx_event, max_idx_event, **kwargs)
    else:                 self.channels = [deepcopy(ch) for ch in self.channels]
    
    for ch in self.channels:
      
      assert ch.name, sys.exit("ERROR: a channel has no name in {}".format(self.name))
      assert ch.form, sys.exit("ERROR: channel {} has no format declared".format(ch.name))


  def load_run(self):
     """
     Load the main run data structures.
     """
     
     if self.channels:
       """
       If channels are provided, read basic attributes from channel 0. Consistency should be
       checked later with other channels.
       """
       for idx, ch in enumerate(self.channels): ch.load_run()
         
       self.min_idx_event, self.max_idx_event = self.channels[0].min_idx_event, self.channels[0].max_idx_event

     else:
       """
       If channels are provided, they should be reading the input files. Otherwise the Run
       class is the main engine. Each run includes its instance of the Event and RunInfo.
       """
       self.load_file()
       self.event    = Event(self.form,   chain = getattr(self, "input_chain", None), record_length = getattr(self, "record_length", 130))
       self.run_info = RunInfo(self.form, hists = getattr(self, "input_hists", None))
   

  def update_event(self):
    """
    Updates content of event class.
    """
    if self.channels:
      for ch in self.channels: ch.update_event()
    
    elif not "root" in self.form:
      self.fill_event(self.event, run_info=self.run_info)
      self.event.validate(run_info=self.run_info)
    
    else:
      self.fill_event(self.event)
      self.event.chain.get_entry(self.event.idx_event)

    return


  def update_run_info(self):
    """
    Updates content of run_info class.
    """
    if self.channels:
      for ch in self.channels: ch.update_run_info()
    
    else:
      self.fill_run_info(self.run_info)
    
    return
  
  def _get_event(self, last=True, reset_ptr_after_read=False):
    """
    "Private" method for reading either the last or first
    event in the file. If the first event is read it will 
    reset the pointer position.
    """

    event = Event(self.form, record_length = getattr(self, "record_length", 130))
    self.fill_event(event, run_info=self.run_info, read_backward=last, reset_ptr_after_read=reset_ptr_after_read)
    
    while not event.validate(run_info=self.run_info):
      self.fill_event(event, run_info=self.run_info, read_backward=last, reset_ptr_after_read=reset_ptr_after_read)
      # debugging purposes
      #last_event.print_info()
    return event
  
  
  def get_n_events(self):
    """
    Get total number of events for the run.
    """
    
    if self.max_idx_event == -1:
    
     if self.channels:
        """
        Just get the minimum amount of events to avoid inconsistencies.
        """
        n_events_list = []
        for ch in self.channels: n_events_list.append( ch.get_n_events() )
        self.max_idx_event = min(n_events_list) - 1 

     elif "root" in self.form:
        self.max_idx_event = self.event.chain.get_entries() - 1

     elif "wc" in self.form:
       last_event = self._get_event()
       self.max_idx_event = int(last_event.header["EVENT"]) - 1

     elif "wd" in self.form:
       first_event = self._get_event(last=False, reset_ptr_after_read=True)
       last_event = self._get_event()
       self.max_idx_event = int(last_event.header["Event Number"]) - int(first_event.header["Event Number"])
     
     elif "cr" in self.form:
       pass

    return self.max_idx_event + 1


  def clear_cache(self):
    """
    Clear cache of the chain proxy if needed.
    """
    
    if self.channels:
      for ch in self.channels: ch.clear_cache()
     
    elif "root" in self.form and getattr(self, "input_chain", None):
      self.event.chain.clear_cache()


  def close_files(self):
    """
    Close files associated with the run.
    """
    
    if self.channels:
      for ch in self.channels: ch.close_files()
    else: 
       self.close_thisfile()


  def validate(self):
    """
    Checks that number of events is consistent among channels.
    This is only useful if channels are read separately.
    """

    if self.channels: 
      
      n_events_list = []
      for ch in self.channels: n_events_list.append( ch.get_n_events() )
    
      if not n_events_list.count(n_events_list[0]) == len(n_events_list): 
        sys.exit("ERROR: event number mis-match among channels")
    
    else:
      pass

    return

# EOF
