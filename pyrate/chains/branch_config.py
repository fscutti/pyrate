#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import ROOT

description="""
This file is a database for branches holding their 
type and initialising their values. Branches are
pure python dictionaries up to this point.
"""

# -------------
# muon DAQ tree
# -------------
branches_dict = {}

branches_dict["record_length"]    = { "value" : np.zeros(1, dtype=int),  "type" : "record_length/s"    }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["event_nr"]         = { "value" : np.zeros(1, dtype=long), "type" : "event_nr/i"         }   # C++ 'unsigned long' (32 bit), ROOT 'i' (UInt_t)
branches_dict["trigger_ts"]       = { "value" : np.zeros(1, dtype=long), "type" : "trigger_ts/i"       }   # C++ 'unsigned long' (32 bit), ROOT 'i' (UInt_t)
#branches_dict["bad_event"]        = { "value" : np.zeros(1, dtype=int),  "type" : "bad_event/b"        }   # C++ 'unsigned char' (8 bit), ROOT 'b' (UChar_t)
#branches_dict["incomplete_event"] = { "value" : np.zeros(1, dtype=int),  "type" : "incomplete_event/b" }   # C++ 'unsigned char' (8 bit), ROOT 'b' (UChar_t)
branches_dict["waveform0"]        = { "value" : ROOT.vector('double')()                                 }   # C++ 'short' (16 bit)
branches_dict["waveform1"]        = { "value" : ROOT.vector('double')()                                 }   # C++ 'short' (16 bit)
branches_dict["waveform2"]        = { "value" : ROOT.vector('double')()                                 }   # C++ 'short' (16 bit)
branches_dict["012_flag"]         = { "value" : np.zeros(1, dtype=int),  "type" : "flag_012/s"         }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["12_flag"]          = { "value" : np.zeros(1, dtype=int),  "type" : "flag_12/s"          }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["01_flag"]          = { "value" : np.zeros(1, dtype=int),  "type" : "flag_01/s"          }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_dict["02_flag"]          = { "value" : np.zeros(1, dtype=int),  "type" : "flag_02/s"          }   # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)


# ----------------------
# muon DAQ metadata tree
# ----------------------

branches_md_dict = {}

branches_md_dict["run_start"]         = { "value" : np.zeros(1, dtype=float), "type" : "run_start/D"        } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["run_end"]           = { "value" : np.zeros(1, dtype=float), "type" : "run_end/D"          } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["012_coin"]          = { "value" : np.zeros(1, dtype=int),   "type" : "coin_012/s"         } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["01_coin"]           = { "value" : np.zeros(1, dtype=int),   "type" : "coin_01/s"          } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["12_coin"]           = { "value" : np.zeros(1, dtype=int),   "type" : "coin_12/s"          } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["02_coin"]           = { "value" : np.zeros(1, dtype=int),   "type" : "coin_02/s"          } # C++ 'unsigned short' (16 bit), ROOT 's' (UShort_t)
branches_md_dict["avg_trigger_rate"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_trigger_rate/D" } # C++ 'double' (64 bit), ROOT 'D' (Float_t)
branches_md_dict["avg_readout_rate"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_readout_rate/D" } # C++ 'double' (64 bit), ROOT 'D' (Float_t)
branches_md_dict["avg_efficiency_0"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_efficiency_0/D" } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["avg_efficiency_1"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_efficiency_1/D" } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["avg_efficiency_2"]  = { "value" : np.zeros(1, dtype=float), "type" : "avg_efficiency_2/D" } # C++ 'double' (64 bit), ROOT 'D' (Double_t)
branches_md_dict["trigger_rates"]     = { "value" : ROOT.vector('float')()                                  } # C++ 'float' (32 bit)
branches_md_dict["readout_rates"]     = { "value" : ROOT.vector('float')()                                  } # C++ 'float' (32 bit)


# EOF
