#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pyrate.classes import Chain
from pyrate.chains import  branch_config as bc

"""
Database for chain configuration. If no branches are passed, it is assumed that the user
will read them off some input file. In this case, it will not be possible to modify the 
content of the branches.
"""

test_chain   = Chain(name = "nT")

# chains for old muon acquisition
# -------------------------------
out_chain    = Chain(name = "muons", branches = bc.branches_dict)
out_md_chain = Chain(name = "run_metadata", branches = bc.branches_md_dict)


# EOF
