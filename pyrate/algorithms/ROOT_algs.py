from pyrate.classes import Algorithm
from ROOT import TH1F, TFile, TGraphErrors, TCanvas
import ROOT
from copy import copy, deepcopy
from array import array

ROOT.gROOT.SetBatch(True)

description="""
In this module we define utilities for handling ROOT objects.
As a policy, algorithms relying on ROOT should have the word "ROOT" in their name.
"""

class FillROOTGraph(Algorithm):
  """
  Instantiate with the list of histograms.
  """
  def __init__(self, name="FillROOTGraph"):
    
    self.name            = name

  def initialize(self):

    self.reco_pos_graph = {}
    self.true_pos_graph = {}
    self.pos_canvas     = {}

    self.pr_store["reco_x"] = {}
    self.pr_store["reco_y"] = {}
    self.pr_store["true_x"] = {}
    self.pr_store["true_y"] = {}

    self.graph_colors = {}
    
    self.colors = [ROOT.kRed, ROOT.kBlue, ROOT.kGreen,
                   ROOT.kYellow, ROOT.kMagenta, ROOT.kGray+3,
                   ROOT.kOrange, ROOT.kPink, ROOT.kViolet,
                   ROOT.kSpring, ROOT.kTeal, ROOT.kCyan+3,
                   ROOT.kAzure, ROOT.kRed+1, ROOT.kBlue+1, 
                   ROOT.kGreen+1, ROOT.kMagenta+1] 

  def execute(self):

    ev_tag = str(self.run.event.xcoord)+"_"+str(self.run.event.ycoord)
    
    if not ev_tag in self.pr_store["reco_x"]: self.pr_store["reco_x"][ev_tag] = []
    if not ev_tag in self.pr_store["reco_y"]: self.pr_store["reco_y"][ev_tag] = []
    if not ev_tag in self.pr_store["true_x"]: self.pr_store["true_x"][ev_tag] = []
    if not ev_tag in self.pr_store["true_y"]: self.pr_store["true_y"][ev_tag] = []
    
    if not ev_tag in self.graph_colors: self.graph_colors[ev_tag] = self.colors.pop()
    
    self.pr_store["reco_x"][ev_tag].append(self.tr_store["reco_x"])
    self.pr_store["reco_y"][ev_tag].append(self.tr_store["reco_y"])
    self.pr_store["true_x"][ev_tag].append(self.tr_store["true_x"])
    self.pr_store["true_y"][ev_tag].append(self.tr_store["true_y"])
    

  def finalize(self):
    
      f_out = TFile("f_"+self.run.name+".root", "RECREATE")
      
      draw_first_ev = True
      draw_first_comp = True

      reco_canvas = TCanvas("c_reco_"+self.run.name, "c_reco_"+self.run.name, 800, 800)
      reco_canvas.SetTickx()      
      reco_canvas.SetTicky()      
      reco_canvas.SetGridx()      
      reco_canvas.SetGridy()      

      for ev in self.pr_store["true_x"]:
        if not ev in self.true_pos_graph:

          self.true_pos_graph[ev] = TGraphErrors(len(self.pr_store["true_x"][ev]))
          self.true_pos_graph[ev].SetNameTitle("true_"+ev)
          self.true_pos_graph[ev].SetMarkerStyle(41)
          self.true_pos_graph[ev].SetMarkerSize(4)
          self.true_pos_graph[ev].SetMarkerColor(self.graph_colors[ev])
          self.true_pos_graph[ev].SetLineColor(self.graph_colors[ev])

          self.reco_pos_graph[ev] = TGraphErrors( len(self.pr_store["reco_x"][ev]) )
          self.reco_pos_graph[ev].SetNameTitle("reco_"+ev)
          self.reco_pos_graph[ev].SetMarkerStyle(31)
          self.reco_pos_graph[ev].SetMarkerSize(4)
          self.reco_pos_graph[ev].SetMarkerColor(self.graph_colors[ev])
          self.reco_pos_graph[ev].SetLineColor(self.graph_colors[ev])

          for i in xrange(len(self.pr_store["true_x"][ev])):
            self.true_pos_graph[ev].SetPoint(i, 
                    float(self.pr_store["true_x"][ev][i]), 
                    float(self.pr_store["true_y"][ev][i]))

          for i in xrange(len(self.pr_store["reco_x"][ev])):
            self.reco_pos_graph[ev].SetPoint(i, 
                    float(self.pr_store["reco_x"][ev][i]), 
                    float(self.pr_store["reco_y"][ev][i]))

          self.true_pos_graph[ev].GetYaxis().SetRangeUser(0., 20.)
          self.true_pos_graph[ev].GetXaxis().SetLimits(0., 20.)
          self.true_pos_graph[ev].GetXaxis().SetTitle("x [cm]")
          self.true_pos_graph[ev].GetYaxis().SetTitle("y [cm]")

          self.reco_pos_graph[ev].GetYaxis().SetRangeUser(0., 20.)
          self.reco_pos_graph[ev].GetXaxis().SetLimits(0., 20.)
          self.reco_pos_graph[ev].GetXaxis().SetTitle("x [cm]")
          self.reco_pos_graph[ev].GetYaxis().SetTitle("y [cm]")
          
          # ---------------
          # writing to file
          # ---------------
          f_out.WriteObject(self.true_pos_graph[ev], self.true_pos_graph[ev].GetName())   
          f_out.WriteObject(self.reco_pos_graph[ev], self.reco_pos_graph[ev].GetName())   
         
          # -------
          # drawing
          # -------
          self.pos_canvas[ev] = TCanvas("c_"+ev+"_"+self.run.name, "c_"+ev+"_"+self.run.name, 800, 800)
          self.pos_canvas[ev].SetTickx()      
          self.pos_canvas[ev].SetTicky()      
          self.pos_canvas[ev].SetGridx()      
          self.pos_canvas[ev].SetGridy()      

          self.pos_canvas[ev].cd()

          if draw_first_ev:
            self.true_pos_graph[ev].Draw("AP")
            self.reco_pos_graph[ev].Draw("P")
          else:
            self.true_pos_graph[ev].Draw("P,same")
            self.reco_pos_graph[ev].Draw("P,same")
            draw_first_ev = False
          
          reco_canvas.cd()
          
          if draw_first_comp:
            self.reco_pos_graph[ev].Draw("AP")
          else:
            self.reco_pos_graph[ev].Draw("P,same")
            draw_first_comp = False

          f_out.WriteObject(self.pos_canvas[ev], self.pos_canvas[ev].GetName())   
          self.pos_canvas[ev].SaveAs(self.pos_canvas[ev].GetName()+".eps")

      f_out.WriteObject(reco_canvas, reco_canvas.GetName())   
      reco_canvas.SaveAs(reco_canvas.GetName()+".eps")
      f_out.Close()



class FillROOTHists(Algorithm):
  """
  Instantiate with the list of histograms.
  """
  def __init__(self, name="FillROOTHists", hlist = [], fill_from_store = True, reload_hists = False):
    
    self.name            = name
    self.hlist           = hlist
    self.fill_from_store = fill_from_store
    self.reload_hists    = reload_hists

  def initialize(self):
    """
    Reload histograms if required. This should be done if a new instance of the 
    histogram is required for each different processed run.
    """
    if self.reload_hists:
      for h in self.hlist:
        self.pr_store[h.name].load_hist()


  def execute(self):
    
    if self.fill_from_store:
      """
      Fills all initialized histograms using variables previously 
      filled in the transient store. The fill_from_store method
      will authomatically search for the name of this variable in 
      the store.
      """
      self.logger.debug("{}: Filling histograms...".format(self.name))
      for h in self.hlist:
        self.pr_store[h.name].fill_from_store(self.tr_store)


class WriteROOTFile(Algorithm):
  
  def __init__(self, name = "WriteROOTFile", outfile = None, hist_list=[], tree_list=[], opt=""):
    
    self.name      = name
    self.outfile   = outfile
    self.hist_list = hist_list
    self.tree_list = tree_list
    self.opt       = opt

  def initialize(self):
    """
    A different output file for each run.
    """
    self.outf   = self.outfile.replace(".root", self.run.name+".root")
    self.pr_store[self.outf] = TFile.Open(self.outf, self.opt)

  def finalize(self):
    """
    Writes histograms or trees and closes the file.
    """
    for h in self.hist_list:
      self.logger.debug("Writing histogram {}".format(h.name))
      self.pr_store[self.outf].WriteObject(self.pr_store[h.name]._hist, h.name)
    
    for t in self.tree_list:
      self.logger.debug("Writing tree {}".format(t.name))
      self.pr_store[self.outf].WriteObject(self.pr_store[t.name]._chain, t.name)
    
    # close the last open output file
    self.pr_store[self.outf].Close()



class FillROOTChain(Algorithm):
  """
  Instantiate with the chain
  """
  def __init__(self, name="FillROOTChain", output_chain=None, reload_chain = False):
    
    self.name  = name
    self.output_chain = output_chain
    self.reload_chain = reload_chain 

  def initialize(self):
    
    """
    If different chains will be created for different runs.
    WARNING: chains are linked to the file they are written to by ROOT. 
    If a persistent chain is written to a file which is then closed, 
    the chain itself will not be accessible anymore.
    """
    if self.reload_chain:
      self.pr_store[self.output_chain].load_chain()
    

  def execute(self):
      
      cname = self.output_chain

      if self.run.form == "root":
        self.tr_store["eventnumber"]  = self.run.event.chain.eventnumber
        self.pr_store[cname].fill_chain("event_nr", self.tr_store["eventnumber"])
      
      if self.run.form == "wd":
        
        if cname == "muons":
          self.pr_store[cname].fill_branch("event_nr", self.tr_store["event_nr"])
          self.pr_store[cname].fill_branch("012_flag", self.tr_store["012_flag"])
          self.pr_store[cname].fill_branch("12_flag", self.tr_store["12_flag"])
          self.pr_store[cname].fill_branch("01_flag", self.tr_store["01_flag"])
          self.pr_store[cname].fill_branch("02_flag", self.tr_store["02_flag"])
          
          self.pr_store[cname].fill_branch("record_length", self.tr_store["record_length"])
          self.pr_store[cname].fill_branch("trigger_ts", self.tr_store["trigger_ts"])
          
          self.pr_store[cname].fill_branch("waveform0", self.tr_store["waveform0"])
          self.pr_store[cname].fill_branch("waveform1", self.tr_store["waveform1"])
          self.pr_store[cname].fill_branch("waveform2", self.tr_store["waveform2"])
          
          self.pr_store[cname].fill_chain() 
     

      if self.run.form == "wc":

        if cname == "muons":
          self.pr_store[cname].fill_branch("waveform0", self.tr_store["ch0_waveform"])
          self.pr_store[cname].fill_branch("waveform1", self.tr_store["ch0_waveform"])
          self.pr_store[cname].fill_branch("waveform2", self.tr_store["ch0_waveform"])
          
          self.pr_store[cname].fill_chain() 
      
  def finalize(self):
    """
    Branches can be filled also at the very end of the loop if they are just metadata.
    """
    
    cname = self.output_chain

    if self.run.form == "wd":

      if cname == "run_metadata":

          self.pr_store[cname].fill_branch("012_coin",         self.pr_store["012_coin"][self.run.name])
          self.pr_store[cname].fill_branch("12_coin",          self.pr_store["12_coin"][self.run.name])
          self.pr_store[cname].fill_branch("01_coin",          self.pr_store["01_coin"][self.run.name])
          self.pr_store[cname].fill_branch("02_coin",          self.pr_store["02_coin"][self.run.name])
          
          self.pr_store[cname].fill_branch("avg_efficiency_0", self.pr_store["avg_efficiency_0"][self.run.name])
          self.pr_store[cname].fill_branch("avg_efficiency_1", self.pr_store["avg_efficiency_1"][self.run.name])
          self.pr_store[cname].fill_branch("avg_efficiency_2", self.pr_store["avg_efficiency_2"][self.run.name])

          self.pr_store[cname].fill_branch("trigger_rates",    self.pr_store["trigger_rates"][self.run.name])
          self.pr_store[cname].fill_branch("readout_rates",    self.pr_store["readout_rates"][self.run.name])

          self.pr_store[cname].fill_branch("avg_readout_rate", self.pr_store["avg_readout_rate"][self.run.name])
          self.pr_store[cname].fill_branch("avg_trigger_rate", self.pr_store["avg_trigger_rate"][self.run.name])

          self.pr_store[cname].fill_branch("run_start",        self.pr_store["run_start"][self.run.name])
          self.pr_store[cname].fill_branch("run_end",          self.pr_store["run_end"][self.run.name])
          
          self.pr_store[cname].fill_chain() 

# EOF
