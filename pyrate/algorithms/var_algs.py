from pyrate.classes import Algorithm
from utils import typecast
import numpy as np
import array
import statistics
#from scipy import stats


description="""
Define here algorithms for computing variables.
"""

class FindPosition(Algorithm):
  
  def __init__(self, name = "FindPosition"):
    self.name = name
    
  
  def initialize(self):
    
    """ 
    Create data structures for metadata
    """ 

    if self.run.form == "cr":
      
      self.pr_store["n_events"] = 0
      self.pr_store["variance"] = {}
      self.pr_store["mean"]     = {}
      
      #"""
      self.pr_store["lt_coord"] = [0.05,  0.05]
      self.pr_store["lb_coord"] = [0.05,  19.95] 
      self.pr_store["rt_coord"] = [19.95, 0.05]
      self.pr_store["rb_coord"] = [19.95, 19.95] 
      #"""

      """
      self.pr_store["lt_coord"] = [0.05,  19.95] 
      self.pr_store["rt_coord"] = [19.95, 19.95]
      self.pr_store["lb_coord"] = [0.05,  0.05]
      self.pr_store["rb_coord"] = [19.95, 0.05]
      """
      #lts = [[0. * xsize,0. * ysize]]
      #rts = [[1. * xsize,0. * ysize]]
      #lbs = [[0. * xsize,1. * ysize]]
      #rbs = [[1. * xsize,1. * ysize]]

  def execute(self):
    
    if self.run.form == "cr":
      
      # order of below vectors matters
      
      ev_tag = str(self.run.event.xcoord)+"_"+str(self.run.event.ycoord)
      
      if not ev_tag in self.pr_store["variance"]: self.pr_store["variance"][ev_tag] = []
      if not ev_tag in self.pr_store["mean"]: self.pr_store["mean"][ev_tag] = []

      weights = [self.run.event.lts, self.run.event.rts, self.run.event.lbs, self.run.event.rbs]
      x_array = [self.pr_store["lt_coord"][0], self.pr_store["rt_coord"][0], self.pr_store["lb_coord"][0], self.pr_store["rb_coord"][0]]
      y_array = [self.pr_store["lt_coord"][1], self.pr_store["rt_coord"][1], self.pr_store["lb_coord"][1], self.pr_store["rb_coord"][1]]

      self.pr_store["variance"][ev_tag].append(weights)
      self.pr_store["mean"][ev_tag].append(weights)
      
      self.tr_store["reco_x"]   = np.average(x_array, weights=weights)
      self.tr_store["reco_y"]   = np.average(y_array, weights=weights)
      
      self.tr_store["true_x"]   = self.run.event.xcoord
      self.tr_store["true_y"]   = self.run.event.ycoord



      self.tr_store["reco"]  = np.array( (self.tr_store["reco_x"], self.tr_store["reco_y"]) )
      self.tr_store["true"]  = np.array( (self.tr_store["true_x"], self.tr_store["true_y"]) )
      self.tr_store["dist"]  = np.linalg.norm( self.tr_store["reco"] - self.tr_store["true"])


  def finalize(self):

      for ev in self.pr_store["variance"]:
          self.pr_store[ev+"wvec"+"_"+"lt"]  = []
          self.pr_store[ev+"wvec"+"_"+"rt"]  = []
          self.pr_store[ev+"wvec"+"_"+"lb"]  = []
          self.pr_store[ev+"wvec"+"_"+"rb"]  = []
          
          self.pr_store[ev+"var"+"_"+"lt"]  = 0.
          self.pr_store[ev+"var"+"_"+"rt"]  = 0.
          self.pr_store[ev+"var"+"_"+"lb"]  = 0.
          self.pr_store[ev+"var"+"_"+"rb"]  = 0.
          
          self.pr_store[ev+"mean"+"_"+"lt"]  = 0.
          self.pr_store[ev+"mean"+"_"+"rt"]  = 0.
          self.pr_store[ev+"mean"+"_"+"lb"]  = 0.
          self.pr_store[ev+"mean"+"_"+"rb"]  = 0.

          for w in self.pr_store["variance"][ev]:
            self.pr_store[ev+"wvec"+"_"+"lt"].append(w[0])
            self.pr_store[ev+"wvec"+"_"+"rt"].append(w[1])
            self.pr_store[ev+"wvec"+"_"+"lb"].append(w[2])
            self.pr_store[ev+"wvec"+"_"+"rb"].append(w[3])

          self.pr_store[ev+"var"+"_"+"lt"] = np.var(self.pr_store[ev+"wvec"+"_"+"lt"], ddof=1)
          self.pr_store[ev+"var"+"_"+"rt"] = np.var(self.pr_store[ev+"wvec"+"_"+"rt"], ddof=1)
          self.pr_store[ev+"var"+"_"+"lb"] = np.var(self.pr_store[ev+"wvec"+"_"+"lb"], ddof=1)
          self.pr_store[ev+"var"+"_"+"rb"] = np.var(self.pr_store[ev+"wvec"+"_"+"rb"], ddof=1)
          
          self.pr_store[ev+"mean"+"_"+"lt"] = np.mean(self.pr_store[ev+"wvec"+"_"+"lt"])
          self.pr_store[ev+"mean"+"_"+"rt"] = np.mean(self.pr_store[ev+"wvec"+"_"+"rt"])
          self.pr_store[ev+"mean"+"_"+"lb"] = np.mean(self.pr_store[ev+"wvec"+"_"+"lb"])
          self.pr_store[ev+"mean"+"_"+"rb"] = np.mean(self.pr_store[ev+"wvec"+"_"+"rb"])
          
          print 
          print 
          print ev+"_"+"lt, mean {}, variance {}".format(self.pr_store[ev+"mean"+"_"+"lt"],self.pr_store[ev+"var"+"_"+"lt"])
          print ev+"_"+"rt, mean {}, variance {}".format(self.pr_store[ev+"mean"+"_"+"rt"],self.pr_store[ev+"var"+"_"+"rt"])
          print ev+"_"+"lb, mean {}, variance {}".format(self.pr_store[ev+"mean"+"_"+"lb"],self.pr_store[ev+"var"+"_"+"lb"])
          print ev+"_"+"rb, mean {}, variance {}".format(self.pr_store[ev+"mean"+"_"+"rb"],self.pr_store[ev+"var"+"_"+"rb"])

      """
      for ev in self.pr_store["variance"]:
          print ev, self.pr_store[ev+"var"+"_"+"lt"]
          print ev, self.pr_store[ev+"var"+"_"+"rt"]
          print ev, self.pr_store[ev+"var"+"_"+"lb"]
          print ev, self.pr_store[ev+"var"+"_"+"rb"]
      """




class CalcVars(Algorithm):
  
  def __init__(self, name = "CalcVars"):
    self.name = name
    
  
  def initialize(self):
    
    # --------------------------------
    # Variables for the metadata tree.
    # --------------------------------
    
    """ 
    Create data structures for metadata
    """ 

    if self.run.form == "wd":
      
      # If we want to monitor all events processed across all runs we will only set
      # the counter to 0 before all runs and events.
      if not "event_count" in self.pr_store:      self.pr_store["event_count"]      = 0
      
      # The following can be refreshed for all new runs...
      self.pr_store["trigger_rates"]    = {}
      self.pr_store["readout_rates"]    = {}
      self.pr_store["avg_trigger_rate"] = {}
      self.pr_store["avg_readout_rate"] = {}
      self.pr_store["run_start"]        = {}
      self.pr_store["run_end"]          = {}
      
      self.pr_store["012_coin"]         = {}
      self.pr_store["12_coin"]          = {}
      self.pr_store["02_coin"]          = {}
      self.pr_store["01_coin"]          = {}
      
      self.pr_store["n_coincidences"]   = {}
      
      self.pr_store["avg_efficiency_0"] = {}
      self.pr_store["avg_efficiency_1"] = {}
      self.pr_store["avg_efficiency_2"] = {}
     

      """
      Fill metadata only once per run and before the event loop is executed
      """
      
      self.pr_store["run_start"][self.run.name] = self.run.channels[0].run_info.info["Run Start"]
      self.pr_store["run_end"][self.run.name]   = self.run.channels[0].run_info.info["Run End"]
      
      self.pr_store["readout_rates"][self.run.name] = []
      self.pr_store["trigger_rates"][self.run.name] = []
      
      for rate in self.run.channels[0].run_info.info["Trg Rate"].split(";"):

        if not rate: continue
        if len(rate.split(":"))<2: continue
        
        ch_readout_rate = float(rate.split("MB/s")[0].replace("Readingat",""))
        ch_trigger_rate = float(rate.split(":")[1].strip("Hz)"))

        self.pr_store["readout_rates"][self.run.name].append(ch_readout_rate)
        self.pr_store["trigger_rates"][self.run.name].append(ch_trigger_rate)

      self.pr_store["avg_readout_rate"][self.run.name] = statistics.mean(self.pr_store["readout_rates"][self.run.name])
      self.pr_store["avg_trigger_rate"][self.run.name] = statistics.mean(self.pr_store["trigger_rates"][self.run.name])


  def execute(self):
    
    if self.run.form == "wc":
      self.tr_store["ch0_waveform"]   = self.run.event.cdict["ch0"]["waveform"]
      self.tr_store["wmax_ch0"]       = max(self.run.event.cdict["ch0"]["waveform"])
      self.tr_store["unixtime"]       = self.run.event.header["UnixTime"]
      self.tr_store["tdc_corrected"]  = self.run.event.header["TDC Corrected"]
      self.tr_store["eventnumber"]    = self.run.event.idx_event + 1
    
    elif self.run.form == "wd":
      
      # ----------------------------
      # Variables for the main tree.
      # ----------------------------
      
      self.tr_store["trigger_ts"]    = self.run.channels[0].event.header["Trigger Time Stamp"]
      self.tr_store["record_length"] = self.run.channels[0].event.record_length
      
      self.tr_store["waveform0"]     = self.run.channels[0].event.waveform
      self.tr_store["waveform1"]     = self.run.channels[1].event.waveform
      self.tr_store["waveform2"]     = self.run.channels[2].event.waveform

      self.pr_store["event_count"] += 1
      self.tr_store["event_nr"]  = self.run.channels[0].event.idx_event + 1
     
    elif self.run.form == "root":
      self.tr_store["eventnumber"]  = self.run.event.chain._chain.eventnumber
      pass
    
    elif self.run.form == "cr":
      pass 


  def finalize(self):
      pass




class CoincidenceFinder(Algorithm):
  """
  Finds coincidences. This is old and not optimised. Needs revision!!!
  """
  def __init__(self, 
             name      = "CoincidenceFinder",
             baseline  = "MODE",
             tolerance = 50,
             MIN_OVER  = 3,
             OFF_SET   = 1,
             REC_LEN   = None,
             ):
  
    self.name      = name
    self.baseline  = baseline
    self.tolerance = tolerance
    self._MIN_OVER = MIN_OVER
    self._OFF_SET  = OFF_SET
    self._REC_LEN  = REC_LEN
    
    self.n_coincidences = {
            "012" : 0,
            "01"  : 0,
            "02"  : 0,
            "12"  : 0,
            "0"   : 0,
            "1"   : 0,
            "2"   : 0
        }
    
    self.coincidence_type = {
                "O12" : False,
                "01"  : False,
                "02"  : False,
                "12"  : False,
                "0"   : False,
                "1"   : False,
                "2"   : False
                }

  def event_cf(self, list0, list1=0, list2=0):

      # Inititize lists with listi[j] != False so can be used for each triple/double/single check
      # Don't need list0 to be initilised because it is called for single events
      # As there is a threshold to be way from 0, getting a 1 is not going to happen

      if not self._REC_LEN:
        """
        Only works for WaveDump now.
        """
        if self.run.channels[0].form == "wd":
          self._REC_LEN = self.run.channels[0].event.record_length


      if list1 == 0:
          list1 = [1]*self._REC_LEN
      if list2 == 0:
          list2 = [1]*self._REC_LEN

      # OK, now check all lists
      try:

          # How many overlapping is wanted
          counter = self._MIN_OVER

          # iterate over triple list
          for i in range(self._REC_LEN):

              # if all non-zero we have found ADC away from the meadian enough to be counted as a a muon event
              if list0[i] and list1[i] and list2[i]:

                  # We have found an overlap. Reduce the counter to mirror
                  counter -= 1

                  # If we have enough events <<in a row>>, then check if they start at the ~same time. If so return true
                  if counter == 0:

                      # now i = start + MINOVER
                      j = i
                      k = i
                      m = i

                      # Find the start of the event for each detector
                      """ ie.
                            |
                      0 0 0 S X X X X 0 0
                      0 0 0 0 S X X X X 0 0
                      0 0 S X X X X 0 0 0
                            |

                      where S is the start would be counted
                      and the position of each S is what we find.
                      """

                      while list0[j]:
                          j -= 1

                      # check not a dummy list
                      while list1[k]:
                          k -= 1
                          # gone though entire list and not found => double/single event
                          if k == 0 or list1[k] != 1:
                              k = j
                              list1[k] = 0

                      while list2[m]:
                          m -= 1
                          if m == 0 or list2[m] != 1:
                              m = j
                              list2[m] = 0

                      # check if they are close enough. If the max and min of the set are close enough.
                      if abs(np.mean([j,k,m])-max([j,k,m])) <= self._OFF_SET and abs(np.mean([j,k,m])-min([j,k,m])) <= self._OFF_SET:
                          return True

              # If we get a lone (random or fake) trigger and the next element in the list is not, need to reset counter
              elif counter != self._MIN_OVER:
                  counter = self._MIN_OVER


      # Catch empty list (make false and won't contribute to run coincidences)
      except IndexError:
          return False

      # If coincidences are not found, return False
      return False
   

  def classify_coincidences(self, master_list, count_dict, coincidence_type_dict):
        """
        This function does this
        Parameters:
        masterlist : a triple entry list. Each of the (3) entries is the ADC from a specific event number from the 3 files. It is normalized to 0 and significant movement away from 0 is treated as an event happening
        count_dict : A dictionary of the inital form {'012': 0, '01' : 0 ...} etc for each of the 7 combinations --> This is updated by the function. It tracks the TOTAL number of coincidences over the whole run.
        coincidence_type_dict : A dictionary of boolans for each single short event ie {'012' : False, '01': False ...} etc. It is updated by the fucntion to correctly classify the coincidences of the single event
        """

        coincidence_type_dict['012']  = self.event_cf(master_list[0], master_list[1], master_list[2])
        count_dict['012']            += coincidence_type_dict['012']

        for i in ['01', '12', '02']:
            # eg i = '01',int(i[0]) = 0
            coincidence_type_dict[i]  = self.event_cf(master_list[int(i[0])], master_list[int(i[1])])
            count_dict[i]            += coincidence_type_dict[i]


        for i in ['0', '1', '2']:
            coincidence_type_dict[i]  = self.event_cf(master_list[int(i)])
            count_dict[i]            += coincidence_type_dict[i]


  def transform_samples(self, samples):
     # 1. Take the baseline for each waveform as the MODE of the samples (i.e., the most frequent value).
     # 2. Transform the waveform by subtracting the baseline.
     # 3. Set all the values closer than 'tolerance' to the baseline to zero.
     #a = np.array(samples)
     #baseline = stats.mode(a)[0][0]
     
     baseline = None
     if self.baseline == "MODE":
       # a faster way to find the mode
       baseline = max(set(samples), key=samples.count) 
     
     samples = [ int(sample) - int(baseline) for sample in samples ]
     samples = [ 0 if abs(int(sample)) < self.tolerance  else sample for sample in samples ]

     return samples

  def initialize(self):
    self.pr_store["n_coincidences"][self.run.name] = self.n_coincidences.fromkeys(self.n_coincidences,0)


  def execute(self):
    
    if self.run.form == "wc":
      pass 
    
    elif self.run.channels[0].form == "wd":
      
      tr_wf_ch0 = self.transform_samples(self.run.channels[0].event.waveform)
      tr_wf_ch1 = self.transform_samples(self.run.channels[1].event.waveform)
      tr_wf_ch2 = self.transform_samples(self.run.channels[2].event.waveform)
      
      self.classify_coincidences([tr_wf_ch0, tr_wf_ch1, tr_wf_ch2], self.pr_store["n_coincidences"][self.run.name], self.coincidence_type)
      
      self.tr_store["012_flag"] = self.coincidence_type["012"] 
      self.tr_store["12_flag"]  = self.coincidence_type["12"] 
      self.tr_store["01_flag"]  = self.coincidence_type["01"] 
      self.tr_store["02_flag"]  = self.coincidence_type["02"] 
    
    elif self.run.form == "root":
      pass

  def finalize(self):

      self.pr_store["012_coin"][self.run.name] = self.pr_store["n_coincidences"][self.run.name].get("012")
      self.pr_store["12_coin"][self.run.name]  = self.pr_store["n_coincidences"][self.run.name].get("12")
      self.pr_store["01_coin"][self.run.name]  = self.pr_store["n_coincidences"][self.run.name].get("01")
      self.pr_store["02_coin"][self.run.name]  = self.pr_store["n_coincidences"][self.run.name].get("02")

      # AT THE END OF THE RUN, CALCULATE THE EFFICIENCY
      # Use the counters to calculate the efficiency!
      # Check for 1/0 errors
      
      eff0 = eff1 = eff2 = 0
      
      if self.pr_store["12_coin"][self.run.name]: eff0 = (1.0 * self.pr_store["012_coin"][self.run.name]) / self.pr_store["12_coin"][self.run.name]
      if self.pr_store["02_coin"][self.run.name]: eff1 = (1.0 * self.pr_store["012_coin"][self.run.name]) / self.pr_store["02_coin"][self.run.name]
      if self.pr_store["01_coin"][self.run.name]: eff2 = (1.0 * self.pr_store["012_coin"][self.run.name]) / self.pr_store["01_coin"][self.run.name]

      # Store the efficiencies in the permanent store.
      # These will be later saved in the metadata tree.
      self.pr_store["avg_efficiency_0"][self.run.name] = eff0
      self.pr_store["avg_efficiency_1"][self.run.name] = eff1
      self.pr_store["avg_efficiency_2"][self.run.name] = eff2

# EOF
