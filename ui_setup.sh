path_of_this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export MAIN=${path_of_this_dir}

add_to_python_path()
{
    export PYTHONPATH=$1:$PYTHONPATH
    echo "  Added $1 to your PYTHONPATH."
}

add_to_path()
{
    export PATH=$1:$PATH
    echo "  Added $1 to your PATH."
}

#eval "setupROOT6.14.04"

echo "  Setting up ROOT..."
setupATLAS > /dev/null
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt" > /dev/null

## ----------------
## python utilities
## ----------------

~/.local/bin/pip2.7 install --user file_read_backwards
~/.local/bin/pip2.7 install --user tqdm
~/.local/bin/pip2.7 install --user colorama
~/.local/bin/pip2.7 install --user scipy
~/.local/bin/pip2.7 install --user statistics

# ----------------
# setup PYTHONPATH
# ----------------

echo "  Setting up your PYTHONPATH."
add_to_python_path ${MAIN}
add_to_python_path ${MAIN}/pyrate
add_to_python_path ${MAIN}/utils
echo "  done."

# ---------------------------------------------
# Add sabre_muons_daq/scripts directory to PATH
# ---------------------------------------------
#echo "  Add scripts to PATH."
#add_to_path ${MAIN}/scripts
#echo "  done."

export DAQ_SCRIPTS=$(pwd)/scripts
if [ -n "${PATH}" ]; then
    export PATH=${DAQ_SCRIPTS}:${PATH}
else
    export PATH=${DAQ_SCRIPTS}
fi

# ---------------------------------------------
# Set local python as python to use
# ---------------------------------------------

echo "  Setting local python path"
export PATH=$HOME/.local/bin:$PATH
export PYTHONPATH=$HOME/.local/lib/python2.7/site-packages:$PYTHONPATH
echo "  done."

#EOF

