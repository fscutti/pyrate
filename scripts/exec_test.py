#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time

from pyrate.jobs import job_WaveCatcher, job_WaveDump, job_ROOTree, job_ROOTHists

"""
Load the list of jobs to be run.
"""

job_list = []
#job_list += [job_WaveDump.job]    
#job_list += [job_WaveCatcher.job] 
job_list += [job_ROOTree.job] 
#job_list += [job_ROOTHists.job] 

if __name__ == "__main__":
  for job in job_list: 
    job.launch()

# EOF
